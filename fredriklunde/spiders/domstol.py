# -*- coding: utf-8 -*-

import random
import string

import arrow
from scrapy import Spider, Selector, Request


class DomstolSpider(Spider):
    name = 'domstol'
    allowed_domains = ['domstol.no']
    start_urls = ['https://www.domstol.no/no/Nar-gar-rettssaken1/Nar-gar-rettssaken/']


    def __init__(self, *args, **kwargs):

        now = arrow.get(arrow.now())

        self.fra_dato = kwargs.get('fra_dato', now.format('DD.MM.YYYY'))
        self.til_dato = kwargs.get('til_dato', now.format('DD.MM.YYYY'))
        self.periode = kwargs.get('periode', '')
        self.domstol = kwargs.get('domstol', 'Alle domstoler')


    def generate_random_string(self):
        return ''.join(random.choices(string.ascii_uppercase + string.ascii_lowercase + string.digits, k=16))

    def parse(self, response):

        view_state = response.xpath('//input[@id="__VIEWSTATE"]/@value').extract_first()
        view_state_generator = response.xpath('//input[@id="__VIEWSTATEGENERATOR"]/@value').extract_first()
        feedback_id = response.xpath('//input[@id="ctl00_BodyContent_FeedbackForm_FeedbackPageId"]/@value').extract_first()

        if self.domstol != 'Alle domstoler':
            self.domstol = response.xpath('//option[text()="'+self.domstol+'"]/@value').extract_first()

        if not self.domstol:
            raise Exception('domstol value is not valid!')

        body_args = {
            'form_id' : self.generate_random_string(),
            'view_state' : view_state,
            'view_state_generator' : view_state_generator,
            'feedback_id' : feedback_id,
            'domstol' : self.domstol,
            'periode' : self.periode,
            'fra_dato' : self.fra_dato,
            'til_dato' : self.til_dato,
            'sok' : 'Søk'
        }

        body = self.generate_body(args=body_args)
        yield Request('https://www.domstol.no/no/Nar-gar-rettssaken1/Nar-gar-rettssaken/',
                      method='POST',
                      headers={
                          'Accept' : 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
                          'Content-Type' : 'multipart/form-data; boundary=----WebKitFormBoundary' + body_args['form_id']
                      },
                      body=body,
                      callback=self.parse_results)

    def parse_results(self, response):
        """ Here you will parse your results"""
        pass

    def generate_body(self, args):

        data = """
------WebKitFormBoundary{form_id}
Content-Disposition: form-data; name="__EVENTTARGET"


------WebKitFormBoundary{form_id}
Content-Disposition: form-data; name="__EVENTARGUMENT"


------WebKitFormBoundary{form_id}
Content-Disposition: form-data; name="__LASTFOCUS"


------WebKitFormBoundary{form_id}
Content-Disposition: form-data; name="__VIEWSTATE"

{view_state}
------WebKitFormBoundary{form_id}
Content-Disposition: form-data; name="__VIEWSTATEGENERATOR"

{view_state_generator}
------WebKitFormBoundary{form_id}
Content-Disposition: form-data; name="ctl00$BodyContent$MainContentRegion$InternalNewsRegion$TextBoxFraDato"

{fra_dato}
------WebKitFormBoundary{form_id}
Content-Disposition: form-data; name="ctl00$BodyContent$MainContentRegion$InternalNewsRegion$TextBoxTilDato"

{til_dato}
------WebKitFormBoundary{form_id}
Content-Disposition: form-data; name="ctl00$BodyContent$MainContentRegion$InternalNewsRegion$DropDownListPeriode"

{periode}
------WebKitFormBoundary{form_id}
Content-Disposition: form-data; name="ctl00$BodyContent$MainContentRegion$InternalNewsRegion$DropDownListDomstol"


------WebKitFormBoundary{form_id}
Content-Disposition: form-data; name="ctl00$BodyContent$MainContentRegion$InternalNewsRegion$ButtonSok"

{sok}
------WebKitFormBoundary{form_id}
Content-Disposition: form-data; name="ctl00$BodyContent$FeedbackForm$FeedbackPageId"

{feedback_id}
------WebKitFormBoundary{form_id}
Content-Disposition: form-data; name="ctl00$BodyContent$FeedbackForm$hPot"


------WebKitFormBoundary{form_id}
Content-Disposition: form-data; name="ctl00$BodyContent$FeedbackForm$FeedbackEmail"


------WebKitFormBoundary{form_id}
Content-Disposition: form-data; name="ctl00$BodyContent$FeedbackForm$FeedbackComment"


------WebKitFormBoundary{form_id}--""".format(**args)

        return data


